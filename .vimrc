filetype plugin indent on

set nocompatible
set autoread
set ignorecase
set smartcase
set hlsearch
set incsearch
set lazyredraw
set showmatch
set mat=2
set ruler
set showcmd
set smartindent
set smarttab
set tabstop=8
set number
set noshowmode
if has('mouse')
	set mouse=a
endif

noremap j k
noremap k j
noremap L $
noremap H ^
noremap s *
noremap S #
noremap J gg
noremap K G
noremap <CR> i
nmap // :nohls<CR>:<BS>
noremap U <C-r>
noremap ` <C-w>
noremap ~ <C-w>w
noremap m `
noremap M m
noremap p "0p
noremap P "0P

let g:netrw_liststyle = 3
let g:netrw_banner = 0
let g:netrw_winsize = 20
let g:netrw_altv = 1
let g:netrw_browse_split = 4

syntax enable
color onedark
