T_F="$(tput setaf $(tput colors))"
T_BLACKF="$(tput setaf 0)"
T_REDF="$(tput setaf 1)"
T_GREENF="$(tput setaf 2)"
T_YELLOWF="$(tput setaf 3)"
T_BLUEF="$(tput setaf 4)"
T_MAGNETAF="$(tput setaf 5)"
T_CYANF="$(tput setaf 6)"
T_WHITEF="$(tput setaf 7)"
T_BOLD="$(tput bold)"
T_DIM="$(tput dim)"
T_UNDER="$(tput smul)"
T_END_UNDER="$(tput rmul)"
T_CLEAR="$(tput sgr0)"

function cdl {
	cd "$1"
	ls
}

function print_fail_status {
	local status="$?"
	[ "$status" -ne 0 ] && echo " $T_REDF[$status]$T_F"
}

function print_git {
	local branch=$(git rev-parse --abbrev-ref HEAD 2> /dev/null)
	[ "$branch" == "" ] && return 1

	local count=($(git diff --numstat 2> /dev/null | awk '
		BEGIN {
			add = 0
			del = 0
		}
		{
			add += $1
			del += $2
		}
		END {
			print add
			print del
		}
	'))
	echo -n " [$branch"
	[ "${count[0]}" -ne 0 ] && echo -n " $T_GREENF+${count[0]}"
	[ "${count[1]}" -ne 0 ] && echo -n " $T_REDF-${count[1]}"
	echo -n "$T_F]"
}	

alias ls='ls -G'
alias grep='grep --color=auto'

umask 077

export PS1="\[$T_BOLD\][\u@\h \w]\$(print_fail_status)\n\$\[$T_CLEAR\] "
export HISTCONTROL=erasedups
